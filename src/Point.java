import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

class Point {
	
	private final Integer x;
	private final Integer y;
	
	//This declaration has to be moved inside the calculateSquares method for it to work in different Point class setup
	//When the declaration is moved inside the method then System.out.println(squares); in main wont work
	static ArrayList<ArrayList<Integer>> squares = new ArrayList<ArrayList<Integer>>();
	
	public Point(Integer x, Integer y) {
		this.x = x;
		this.y = y;
	}
	
	public Integer getX() {
		return x;
	}
	
	public Integer getY() {
		return y;
	}
	
	public static int calculateSquares(List<Point> points) {
		
		int countSquares = 0;
		
		for (int pnt1 = 0; pnt1 < points.size(); pnt1++) {
			
			for (int pnt2 = 0; pnt2 < points.size(); pnt2++) {
				
				if (pnt2 != pnt1) {
					
					for (int pnt3 = 0; pnt3 < points.size(); pnt3++) {
						
						if (pnt3 != pnt2 && pnt3 != pnt1) {
							
							for (int pnt4 = 0; pnt4 < points.size(); pnt4++) {
								
								if (pnt4 != pnt3 && pnt4 != pnt2 && pnt4 != pnt1) {
									
									Point p1 = points.get(pnt1);
									Point p2 = points.get(pnt2);
									Point p3 = points.get(pnt3);
									Point p4 = points.get(pnt4);

									int d2 = (p1.getX() - p2.getX()) * (p1.getX() - p2.getX()) + (p1.getY() - p2.getY()) * (p1.getY() - p2.getY());
									int d3 = (p1.getX() - p3.getX()) * (p1.getX() - p3.getX()) + (p1.getY() - p3.getY()) * (p1.getY() - p3.getY());
									int d4 = (p1.getX() - p4.getX()) * (p1.getX() - p4.getX()) + (p1.getY() - p4.getY()) * (p1.getY() - p4.getY());
									int d23 = (p2.getX() - p3.getX()) * (p2.getX() - p3.getX()) + (p2.getY() - p3.getY()) * (p2.getY() - p3.getY());
									int d24 = (p2.getX() - p4.getX()) * (p2.getX() - p4.getX()) + (p2.getY() - p4.getY()) * (p2.getY() - p4.getY());
									int d34 = (p3.getX() - p4.getX()) * (p3.getX() - p4.getX()) + (p3.getY() - p4.getY()) * (p3.getY() - p4.getY());

									if (d2 == d3 && 2 * d2 == d4 && 2 * d2 == d23) {
										
										int d = d24;
										if (d == d34 && d == d2) {
											
											ArrayList<Integer> array = new ArrayList<Integer>();
											array.add(pnt1);
											array.add(pnt2);
											array.add(pnt3);
											array.add(pnt4);
											Collections.sort(array);
											boolean newSquare = true;
											for (int i = 0; i < squares.size(); i++) {
												
												if (array.equals(squares.get(i))) {	
													
													newSquare = false;
												}
											}
											if (newSquare == true) {
												
												squares.add(array);
												countSquares++;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return countSquares;
	}

	public static void main(String[] args) {

		List<Point> list = new ArrayList<Point>();
		list = Arrays.asList(
				new Point(-1, -1),
				new Point(0, 1),
				new Point(1, -2),
				new Point(2, 0),
				new Point(3, 2),
				new Point(4, -1));
		
		List<Point> list1 = new ArrayList<Point>();
		list1 = Arrays.asList(
				new Point(-2, -2),
				new Point(0, -2),
				new Point(2, -2),
				new Point(-2, 0),
				new Point(0, 0),
				new Point(2, 0),
				new Point(-2, 2),
				new Point(0, 2),
				new Point(2, 2));
		
		System.out.println(calculateSquares(list1));
		System.out.println(squares);
	}
}


